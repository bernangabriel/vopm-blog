# VOPM BLog (Test)
VOPM-Blog es una aplicación web basada en AngularJS que utiliza un “local json-server restfull api” para la administración de publicaciones, comentarios, usuarios, etc. Esta aplicación web utiliza una estructura de folders “by features” mediante el cual permite que nuestro código javascript sea más legible y limpio, además facilita el proceso de “test”.


## Estructura de Archivos
El proyecto está compuesto por la siguiente estructura:

### Folders
* `/dist`  Contiene los archivos compilados y minificados (vendors, bundles) del proyecto
* `/src`  Contiene todo el código fuente antes de ser compilado en el folder “dist”

### Archivos
* `.bowerrc` - Archivo de configuración para bower
* `.gitignore` - Archivos con los folders a ser excluidos (node_modules, bower_components, etc)
* `bower.json` - Archivo .json con los assets que serán usados en la aplicación
* `db.json` - Archivo .json utilizado por el servicio “server-json db.json” para crear nuestro propio restfull-api basado en “server-json”
* `gulpfile.js` - Archivo javascript utilizado como corredor de tareas para el proceso de generación de minificados, concatenaciones, templatecache, vendors, bundles que serán utilizado en el forlder “dist”
* `package.json` - Carga los “dev-dependencies” para gulp.JS
* `README.md` - Archivo de texto que contiene la descripción general del proyecto e información de instalación, configuración y despliegue del proyecto

## Instalación y Configuración
Para la instalación y configuración del proyecto realizamos lo siguiente:
1.	Instalar nodeJS
2.	Clonar el repositorio
3.	Ejecutar “npm install” para instalar todos los “dev-dependencies” que serán utilizados por gulp.JS
4.	Ejecutar “bower install” para instalar todos los “assets” requeridos por nuestra aplicación
5.	Ejecutamos el comando “gulp” (para esto necesitamos tener instalado gulp de manera global “npm install -g gulp”)
6.	Correr nuestro resfull api (primero instalamos de manera global json-server “npm install -g json-server”), luego ejecutamos el comando “json-server db.json” para iniciar nuestro restfull api.

